const express = require('express');
const { Pool } = require('pg');
const redis = require('redis');
const { promisify } = require('util');

const app = express();
const port = 3000;

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
});

const redisClient = redis.createClient({
  url: process.env.REDIS_URL,
});

const getAsync = promisify(redisClient.get).bind(redisClient);
const setAsync = promisify(redisClient.set).bind(redisClient);

app.use(express.json());

app.post('/message', async (req, res) => {
  const { message } = req.body;
  await setAsync('message', message);
  res.send('Message stored in Redis');
});

app.get('/message', async (req, res) => {
  try {
    const message = await getAsync('message');
    res.send(`Message from Redis: ${message}`);
  } catch (error) {
    res.status(500).send('Error retrieving message from Redis');
  }
});

app.post('/user', async (req, res) => {
  const { name, email } = req.body;
  const client = await pool.connect();
  try {
    const result = await client.query('INSERT INTO users(name, email) VALUES($1, $2) RETURNING *', [name, email]);
    res.json(result.rows[0]);
  } finally {
    client.release();
  }
});

app.get('/users', async (req, res) => {
  const client = await pool.connect();
  try {
    const result = await client.query('SELECT * FROM users');
    res.json(result.rows);
  } finally {
    client.release();
  }
});

app.get('/user/:id', async (req, res) => {
  const { id } = req.params;
  const client = await pool.connect();
  try {
    const result = await client.query('SELECT * FROM users WHERE id = $1', [id]);
    if (result.rows.length > 0) {
      res.json(result.rows[0]);
    } else {
      res.status(404).send('User not found');
    }
  } finally {
    client.release();
  }
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
